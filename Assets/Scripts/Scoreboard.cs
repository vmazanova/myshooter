﻿using System.Collections;
using UnityEngine;

public class Scoreboard : MonoBehaviour {

    [SerializeField]
    private GameObject playerScoreboardItemPrefab;

    [SerializeField]
    private Transform playerListParent;


    void OnEnable () {
        Player[] players = GameManager.GetAllPlayers();

        foreach(Player player in players) {
            GameObject _playerScoreboardItemGO = Instantiate(playerScoreboardItemPrefab, playerListParent);
            PlayerScoreboardItem _playerScoreboardItem = _playerScoreboardItemGO.GetComponent<PlayerScoreboardItem>();

            if (_playerScoreboardItem != null) {
                _playerScoreboardItem.Setup(player.username, player.kills, player.deaths);
            }
        }
    }
	
	void OnDisable () {
        foreach (Transform child in playerListParent) {
            Destroy(child.gameObject);
        }
    }
}

﻿using System.Collections;
using UnityEngine;
using DatabaseControl;
using UnityEngine.SceneManagement;

public class UserAccountManager : MonoBehaviour {

    public static UserAccountManager instance;

    void Awake () {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);
    }

    public static string PlayerUsername { get; protected set; }
    public static string PlayerPassword { get; protected set; }
    
    public static bool IsLoggedIn { get; protected set; }

    public string loggedInSceneName = "Lobby";
    public string loggedOutSceneName = "LoginMenu";

    public delegate void OnDataReceivedCallback(string data);

    public void LogOut() {
        PlayerUsername = "";
        PlayerPassword = "";

        IsLoggedIn = false;

        SceneManager.LoadScene(loggedOutSceneName);
    }

    public void LogIn(string username, string password) {
        PlayerUsername = username;
        PlayerPassword = password;

        IsLoggedIn = true;

        SceneManager.LoadScene(loggedInSceneName);
    }

    IEnumerator GetData(OnDataReceivedCallback onDataReceived) {
        string data = "ERROR";

        IEnumerator e = DCF.GetUserData(PlayerUsername, PlayerPassword); // << Send request to get the player's data string. Provides the username and password
        while (e.MoveNext()) {
            yield return e.Current;
        }
        string response = e.Current as string; // << The returned string from the request

        if (response == "Error") {
            Debug.Log("Getting data error");
        } else {
            data = response;
        }
        
        if (onDataReceived != null) {
            onDataReceived.Invoke(data);
        }
    }
    IEnumerator SetData(string data) {
        IEnumerator e = DCF.SetUserData(PlayerUsername, PlayerPassword, data); // << Send request to set the player's data string. Provides the username, password and new data string
        while (e.MoveNext()) {
            yield return e.Current;
        }
        string response = e.Current as string; // << The returned string from the request

        if (response != "Success") {
            PlayerUsername = "";
            PlayerPassword = "";
            Debug.Log("Setting data error");
        }
    }

    public void SaveData(string data) {
        if (IsLoggedIn) {
            StartCoroutine(SetData(data));
        }
    }
    public void LoadData(OnDataReceivedCallback onDataReceived) {
        if (IsLoggedIn) {
            StartCoroutine(GetData(onDataReceived));
        }
    }

}

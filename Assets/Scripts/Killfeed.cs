﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killfeed : MonoBehaviour {

    [SerializeField]
    private GameObject killfeedItemPrefab;


    void Start () {
        GameManager.instance.onPlayerKilledCallback += OnKill;
    }

    public void OnKill(string player, string source) {
        GameObject _killfeedItemGO = Instantiate(killfeedItemPrefab, this.transform);
        KillfeedItem _killfeedItem = _killfeedItemGO.GetComponent<KillfeedItem>();
        if (_killfeedItem != null) {
            _killfeedItem.Setup(player, source);
        }

        _killfeedItemGO.transform.SetAsFirstSibling();

        Destroy(_killfeedItemGO, 5f);
    }
}
